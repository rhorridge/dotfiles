#!/usr/bin/env bash
# Copyright (C) 2019-2022 Richard Horridge <rhorridge@hotmail.co.uk>
#
# This file is part of `dotfiles' - Configuration files for various
# applications.

# /etc/skel/.bashrc
#
# This file is sourced by all *interactive* bash shells on startup,
# including some apparently interactive shells such as scp and rcp
# that can't tolerate any output.  So make sure this doesn't display
# anything or bad things will happen !


# Test for an interactive shell.  There is no need to set anything
# past this point for scp and rcp, and it's important to refrain from
# outputting anything in those cases.
if [[ $- != *i* ]] ; then
	# Shell is non-interactive.  Be done now!
	return
fi

# Put your fun stuff here.

# shellcheck disable=SC2006
# shellcheck disable=SC2181
PROMPT="\
\[\033[38;5;012m\]┌─[\
`if [[ \$? = "0" ]];\
	then echo '\[\033[32m\]=)';\
else echo '\[\033[31m\]=(\]';\
fi`\
\[\033[38;5;012m\]\
][\
\[\033[01;35m\]\t\
\[\033[38;5;012m\]\
][\
\[\033[01;32m\]\u\
\[\033[38;5;012m\]]\
@[\[\033[01;31m\]\
`if [ -n \"$SSH_CLIENT\" ] || [ -n \"$SSH_TTY\" ];\
	then echo 'ssh://';\
fi`\
\[\033[01;33m\]\H\
\[\033[38;5;012m\]]\
\[\033[36;5;25m\]\
\[\033[38;5;012m\]\
[\[\033[38;5;63m\]\w\[\033[38;5;012m\]]\n\
\[\033[38;5;012m\]└─\[\033[38;5;27m\]$ \[\e[0m\]"
export PS1="${PROMPT}"

if [[ -n "$SSH_CONNECTION" ]]; then
    export PINENTRY_USER_DATA="USE_CURSES=1"
fi

function upd () {
    # Update the system
    sudo apt update && sudo apt upgrade
}

function inf () {
    # Print system information
    uname -sr && uptime| sed 's/ //' && sensors|grep Pack && \
        lscpu|grep 'CPU MHz:' && acpi && \
				printf 'Memory in use: ' && free -m|grep Mem|\
						awk '{print \$3+\$5\" megs\"}'
}
. "$HOME/.cargo/env"


# Add this code to your .bashrc to save persistent history
#
# See http://eli.thegreenplace.net/2013/06/11/keeping-persistent-history-in-bash
# for details.
#
# Note, HISTTIMEFORMAT has to be set and end with at least one space; for
# example:
#
#   export HISTTIMEFORMAT="%F %T  "
#
# If your format is set differently, you'll need to change the regex that
# matches history lines below.
#
# Eli Bendersky (http://eli.thegreenplace.net)
# This code is in the public domain

export HISTTIMEFORMAT="%F %T  "

log_bash_persistent_history()
{
  local rc=$?
  [[ $(history 1) =~ ^\ *[0-9]+\ +([^\ ]+\ [^\ ]+)\ +(.*)$ ]]
  local date_part="${BASH_REMATCH[1]}"
  local command_part="${BASH_REMATCH[2]}"
  if [ "$command_part" != "$PERSISTENT_HISTORY_LAST" ]
  then
    echo $date_part "|" "$command_part" >> ~/.persistent_history
    export PERSISTENT_HISTORY_LAST="$command_part"
  fi
}

# Stuff to do on PROMPT_COMMAND
run_on_prompt_command()
{
    log_bash_persistent_history
}

if [ "$PROMPT_COMMAND" = "" ]
then
    PROMPT_COMMAND="run_on_prompt_command"
else
    PROMPT_COMMAND="run_on_prompt_command; ""$PROMPT_COMMAND"
fi

complete -C /home/rgh/.local/bin/terraform terraform

complete -C /usr/bin/tofu tofu
