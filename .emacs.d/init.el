;; The default is 800 kilobytes.  Measured in bytes.
(setq gc-cons-threshold (* 50 1000 1000))

;; Profile emacs startup
(add-hook 'emacs-startup-hook
          (lambda ()
            (message "*** Emacs loaded in %s with %d garbage collections."
                     (format "%.2f seconds"
                             (float-time
                              (time-subtract after-init-time before-init-time)))
                     gcs-done)))

;; Get rid of tabs
(setq-default indent-tabs-mode nil)
(setq indent-tabs-mode nil)
(eval-and-compile
  (defvar home-directory (concat (or (getenv "HOME") (expand-file-name (concat "/home" (user-login-name)))) "/")
    "Home directory on UNIX-like systems."))

;; Set load paths
(setq dotfiles-dir (file-name-directory (or (buffer-file-name)
                                            load-file-name)))

;; Set some secret variables
(setq user-full-name (getenv "USER_FULL_NAME")
      user-email-address (getenv "EMAIL_ADDRESS")
      university-email-address (getenv "UNIVERSITY_EMAIL_ADDRESS"))

;; Get rid of custom variables
(let ((path (concat home-directory ".emacs.d/custom.el")))
  (unless (file-exists-p path)
    (with-temp-buffer (write-file path)))
  (setq custom-file path)
  (load-file custom-file))

;; Initialize package sources
(require 'package)

(setq package-archives '(("melpa" . "https://melpa.org/packages/")
                         ("melpa-stable" . "https://stable.melpa.org/packages/")
                         ("nongnu" . "https://elpa.nongnu.org/nongnu/")
                         ("elpa" . "https://elpa.gnu.org/packages/")))

(unless package-archive-contents
  (package-refresh-contents))

;; Initialize use-package on non-Linux platforms
(unless (package-installed-p 'use-package)
  (package-install 'use-package))
(require 'use-package)
(add-to-list 'package-selected-packages 'use-package)

;; Uncomment this to get a reading on packages that get loaded at startup
(setq use-package-verbose t)

;; On non-Guix systems, "ensure" packages by default
(setq use-package-always-ensure t)

(defvar user-functions-directory
  (concat user-emacs-directory "lisp/functions/")
  "Directory beneath which additional per-user Functions are placed.
See also `load-function'.")

(defun load-function (fun)
  "Load a Lisp file defined in `user-functions-directory'."
  (setq default-directory user-functions-directory)
  (interactive "fFunction: ")
  (load-file fun))

(defmacro load--function (fun)
  "Non-interactively load a Lisp file defined in `user-functions-directory'."
  `(load-file (concat ,user-functions-directory (symbol-name ,fun) ".el")))

(defvar user-kbd-macros-directory
  (concat user-emacs-directory "lisp/macros/")
  "Directory beneath which additional per-user keyboard macros are placed.
See also `load-kbd-macro'.")

(defun load-kbd-macro (macro)
  "Load a Lisp file defined in `user-kbd-macros-directory'."
  (setq default-directory user-kbd-macros-directory)
  (interactive "fKbd Macro: ")
  (load-file macro))

(defmacro load--kbd-macro (macro)
  "Non-interactively load a Lisp file defined in `user-macros-directory'."
  `(load-file (concat ,user-macros-directory (symbol-name ,macro) ".el")))

(defvar kbd-macro-dir
  "~/.emacs.d/lisp/macros/"
  "Default directory to save keyboard macros. See `save-last-kbd-macro'.")

(setq kbd-macro-dir (concat user-emacs-directory "lisp/macros/"))

(defmacro verify--kbd-macro-symbol (symbol)
  "Verify that environment is valid for SYMBOL to be a keyboard macro name"
  `(or last-kbd-macro
       (user-error "No keyboard macro defined"))
  `(and (fboundp ,symbol)
        (not (stringp (symbol-function ,symbol)))
        (not (vectorp (symbol-function ,symbol)))
        (user-error "Function %s is already defined and not a keyboard macro"
                    ,symbol))
  `(if (string-equal ,symbol "")
       (user-error "No command name given")))

;; FIXME does not create directories
(defun save-last-kbd-macro (name)
  "Save last entered keyboard macro to `kbd-macro-dir' as NAME."
  (interactive "SMacro Name? ")
  (verify--kbd-macro-symbol name)
  (fset name last-kbd-macro)
  (with-temp-file (concat kbd-macro-dir
                          (symbol-name name)
                          ".el")
    (insert-kbd-macro name)))

(setq mouse-wheel-scroll-amount '(1 ((shift) . 1)))
(setq mouse-wheel-progressive-speed nil)
(setq mouse-wheel-follow-mouse 't)
(setq scroll-step 1)
(setq use-dialog-box nil)

(setq display-time-format "%l:%M %p %b %y"
      display-time-default-load-average nil)

(use-package diminish)
(use-package smart-mode-line
  :config
  (setq sml/no-confirm-load-theme t)
  (sml/setup)
  (sml/apply-theme 'respectful)  ; Respect the theme colors
  (setq sml/mode-width 'right
        sml/name-width 60)
  (setq-default mode-line-format
                `("%e"
                  mode-line-front-space
                  evil-mode-line-tag
                  mode-line-mule-info
                  mode-line-client
                  mode-line-modified
                  mode-line-remote
                  mode-line-frame-identification
                  mode-line-buffer-identification
                  sml/pos-id-separator
                  (vc-mode vc-mode)
                  " "
                                        ;mode-line-position
                  sml/pre-modes-separator
                  mode-line-modes
                  " "
                  mode-line-misc-info))

  (setq rm-excluded-modes
        (mapconcat
         'identity
                                        ; These names must start with a space!
         '(" GitGutter" " MRev" " company"
           " Helm" " Undo-Tree" " Projectile.*" " Z" " Ind"
           " Org-Agenda.*" " ElDoc" " SP/s" " cider.*")
         "\\|")))
(use-package minions
  :hook (doom-modeline-mode . minions-mode))
(use-package doom-modeline
  :ensure t
  :init (doom-modeline-mode 1)
  :custom
  (doom-modeline-height 15)
  )

(use-package doom-themes :ensure t)
(load-theme 'doom-palenight)

(defun rgh/max-screen-width ()
  (nth 3 (car (cl-remove-if #'null (mapcar (lambda (elem)
                                             (when (cdr (assoc 'frames elem))
                                               (assoc 'geometry elem)))
                                           (display-monitor-attributes-list))))))

(eval-when-compile
  (if (equal system-type 'windows-nt)
      (setq bdf-directory-list (concat "C:/users/" (user-login-name) "/.emacs.d/fonts"))
    (setq bdf-directory-list "/usr/share/fonts")))

(set-face-attribute
 'default nil
 :font "Fira Code"
 :height (/ (rgh/max-screen-width) 10))

(use-package all-the-icons)
(use-package all-the-icons-dired)

(use-package counsel
  :ensure t
  :bind (("M-x" . counsel-M-x)
         ("C-x b" . counsel-switch-buffer)
         ("C-x C-b" . counsel-switch-buffer)
         ("C-x C-f" . counsel-find-file)
         :map minibuffer-local-map
         ("C-r" . 'counsel-minibuffer-history)))

(use-package helpful
  :ensure t
  :custom
  (counsel-describe-function-function #'helpful-callable)
  (counsel-describe-variable-function #'helpful-variable)
  :bind
  ([remap describe-function] . counsel-describe-function)
  ([remap describe-command] . helpful-command)
  ([remap describe-variable] . counsel-describe-variable)
  ([remap describe-key] . helpful-key))

;; Test for Windows
(eval-when-compile
  (if (equal system-type 'windows-nt)
      (setq home-directory (concat "C:/users/" (user-login-name) "/"))
    (progn
      (setq default-directory home-directory)
      (setq x-super-keysym 'meta))))

(setq default-directory home-directory)

(add-to-list 'load-path "~/.emacs.d/lisp/")

(setq org-export-backends (quote (ascii html latex odt beamer md taskjuggler )))
(use-package org
  :ensure t
  :mode ("\\.\\(org\\|txt\\)$" . org-mode)
  :init
  (add-hook 'org-mode-hook 'auto-fill-mode)
  (setq org-directory
        (concat home-directory
                "doc/personal/notes/"))
  (setq org-default-notes-file
        (concat org-directory
                "notes.org"))
  (setq org-capture-templates
        `(("a" "Appointment" entry
           (file ,(concat org-directory "refile.org"))
           "* APPOINTMENT: %? :APPOINTMENT: \n%U" :clock-in t :clock-resume t)
          ("t" "Task" entry
           (file ,(concat org-directory "refile.org"))
           "* TODO %?\n%U\n%a\n" :clock-in t :clock-resume t)
          ("n" "Note" entry
           (file ,(concat org-directory "refile.org"))
           "* %? :NOTE:\n%U\n%a\n" :clock-in t :clock-resume t)
          ("p" "Phone Call" entry
           (file ,(concat org-directory "refile.org"))
           "* PHONE %? :PHONE:\n%U\n%a\n" :clock-in t :clock-resume t)
          ("c" "Contact" entry (file ,(concat home-directory "doc/personal/contact/contacts.org"))
           "* %?
    :PROPERTIES:
    :NAME:
    :EMAIL:
    :BIRTHDAY:
    :PHOTO:
    :ADDRESS:
    :PHONE:
    :PHONE:
    :TITLE:
    :ROLE:
    :ORG:
    :URL:
    :NOTE:
    :REV: %t
    :END:
    " :prepend nil)))
  (defvar uni-directory (expand-file-name (concat home-directory "doc/work/university/birmingham/doc/"))
    "University document directory")
  (use-package ob-plantuml
    :ensure nil
    :init
    (setq org-plantuml-jar-path "/usr/share/plantuml/plantuml.jar"))
  (org-babel-do-load-languages 'org-babel-load-languages
                               '((emacs-lisp . t)
                                 (sql . t)
                                 (sqlite . t)
                                 (python . t)
                                 (shell . t)
                                 (lisp . t)
                                 (plantuml . t)
                                 (octave . t)
                                 (R . t)
                                 (C . t)
                                 (gnuplot . t)
                                 (awk . t)
                                 (maxima . t)))
  (setq org-babel-default-header-args '((:session . "none")
                                        (:results . "output")
                                        (:exports . "result")
                                        (:cache . "yes")
                                        (:noweb . "no")
                                        (:hlines . "no")
                                        (:tangle . "no")
                                        (:eval . "never-export")))
  (setq org-todo-keywords
        (quote ((sequence "TODO(t)" "NEXT(n)" "|" "DONE(d)")
                (sequence "WAITING(w@/!)" "HOLD(h@/!)" "|" "SOMEDAY(s@/!)" "CANCELLED(c@/!)"
                          "PHONE" "MEETING" ))))
  (setq org-todo-keyword-faces
        (quote (("TODO" :foreground "red" :weight bold)
                ("NEXT" :foreground "blue" :weight bold)
                ("DONE" :foreground "forest green" :weight bold)
                ("WAITING" :foreground "orange" :weight bold)
                ("HOLD" :foreground "magenta" :weight bold)
                ("CANCELLED" :foreground "forest green" :weight bold)
                ("MEETING" :foreground "forest green" :weight bold)
                ("PHONE" :foreground "forest green" :weight bold)
                ("SOMEDAY" :foreground "yellow" :weight bold))))
  (setq org-use-fast-todo-selection t)
  (setq org-todo-state-tags-triggers
        (quote (("CANCELLED" ("CANCELLED" . t))
                ("WAITING" ("WAITING" . t))
                ("HOLD" ("WAITING") ("HOLD" . t))
                (done ("WAITING") ("HOLD"))
                ("TODO" ("WAITING") ("CANCELLED") ("HOLD"))
                ("NEXT" ("WAITING") ("CANCELLED") ("HOLD"))
                ("DONE" ("WAITING") ("CANCELLED") ("HOLD")))))
  (setq org-refile-targets (quote ((nil :maxlevel . 9)
                                   (org-agenda-files :maxlevel . 9))))
  (setq org-refile-allow-creating-parent-nodes (quote confirm))
  :config
  (setq org-ellipsis " ▾")
  (global-unset-key (kbd "C-c SPC"))
  (load--function 'org/org-agenda-ext)
  (setq org-agenda-custom-commands nil)
  (add-to-list
   'org-agenda-custom-commands
   '("d" "daily start"
     ((todo ""
            ((org-agenda-span 'day)
             (org-agenda-skip-function
              '(org-agenda-skip-entry-if 'not-regexp ":start"))
             (org-agenda-max-entries 5)
             (org-agenda-cmp-user-defined (org-compare-randomly))
             (org-compare-random-refresh t)
             (org-agenda-sorting-strategy '(user-defined-up)))))))
  (add-to-list
   'org-agenda-custom-commands
   '("M" "Monthly agenda"
     ((agenda ""
              ((org-agenda-span 'month))))))
  (add-to-list
   'org-agenda-custom-commands
   '("b" "Buffer summary"
     ((todo ""
            ((org-agenda-files (list (buffer-file-name))))))))
  (add-to-list
   'org-agenda-custom-commands
   '("s" todo "SOMEDAY"))
  (unless (boundp 'org-latex-classes)
    (setq org-latex-classes nil))
  (setq org-export-in-background nil)
  (add-to-list 'org-latex-classes
               '("org-book"
                 "\\documentclass[11pt,a4paper]{book}
[PACKAGES]
[EXTRA]
\\usepackage{pdfpages}
\\hypersetup{linkcolor=blue, pdfborder=0 0 0}"
                 ("\\chapter{%s}" . "\\chapter*{%s}")
                 ("\\section{%s}" . "\\section*{%s}")
                 ("\\subsection{%s}" . "\\subsection*{%s}")
                 ("\\subsubsection{%s}" . "\\subsubsection*{%s}")
                 ("\\paragraph{%s}" . "\\paragraph*{%s}")
                 ("\\subparagraph{%s}" . "\\subparagraph*{%s}")))
  (add-to-list 'org-latex-classes
               '("org-poster"
                 "\\documentclass[presentation]{beamer}
[DEFAULT-PACKAGES]
[PACKAGES]
[EXTRA]
\\usepackage [orientation=portrait, size=a1, scale=1.4]{beamerposter}
\\usepackage [absolute, overlay]{textpos}
\\usepackage{xcolor}
\\renewcommand{\\maketitle}{%
\\begin{textblock}{.1}(0.25,0.75)
\\includegraphics[height=3.5em]{/home/rgh/ln/uni-b-modules/project/doc/project-poster/templates/svg/logo.pdf}
\\end{textblock}
\\begin{textblock}{11}(5,0.25)
\\begin{center}%
\\textcolor{white}{%
\\Huge\\inserttitle\\\\%
\\LARGE\\insertauthor\\\\%
\\Large\\insertinstitute%
}%
\\end{center}%
\\end{textblock}
\\begin{textblock}{.2}(10,15.25)
\\includegraphics[height=1.5em]{/home/rgh/ln/uni-b-modules/project/doc/project-poster/templates/svg/bham-ac-uk.pdf}
\\end{textblock}
\\vspace{13ex}
}
\\setbeamerfont{block title}{size=\\LARGE}
\\hypersetup{linkcolor=blue, pdfborder=0 0 0}"
                 ("\\chapter{%s}" . "\\chapter*{%s}")
                 ("\\section{%s}" . "\\section*{%s}")
                 ("\\subsection{%s}" . "\\subsection*{%s}")
                 ("\\subsubsection{%s}" . "\\subsubsection*{%s}")
                 ("\\paragraph{%s}" . "\\paragraph*{%s}")
                 ("\\subparagraph{%s}" . "\\subparagraph*{%s}")))
  (setq org-latex-image-default-width "0.5\\paperwidth")
  (org-add-link-type
   "latex" nil
   (lambda (path desc format)
     (cond
      ((eq format 'html)
       (format "<span class=\"%s\">%s</span>" path desc))
      ((eq format 'latex)
       (format "\\%s{%s}" path desc)))))
  (setq-default org-display-custom-times t)
  (setq org-time-stamp-custom-formats
        '("<%d %b %Y>" . "<%d/%m/%y %a %H:%M>"))
  (setq org-refile-target-verify-function 'bh/verify-refile-target)
  (load--function 'org/org-contacts)
  :bind
  ("C-c /" . org-sparse-tree))
(use-package org-bullets
  :ensure t
  :after org
  :hook (org-mode . org-bullets-mode)
  :custom
  (org-bullets-bullet-list '("◉" "○" "●" "○" "●" "○" "●")))

(use-package org-contrib
  :config
  (require 'ox-extra)
  (ox-extras-activate '(ignore-headlines)))

(defun single-lines-only ()
  "Replace multiple blank lines with a single one"
  (interactive)
  (goto-char (point-min))
  (while (re-search-forward "\\(^\\s-*$\\)\n" nil t)
    (replace-match "\n")
    (forward-char 1)))

(defun num-todo-items ()
  "Get the number of TODO items that there are currently"
  (interactive)
  (save-excursion
    (switch-to-buffer "*Org Agenda*")
    (princ (- (car (page--count-lines-page)) 9))))

(use-package async
  :ensure
  :demand
  :init
  (setq async-bytecomp-allowed-packages '(all))
  :config
  (async-bytecomp-package-mode 1))

(setq backup-directory-alist `(("." . ,(concat
                                        home-directory
                                        ".local/var/backup/emacs"))))

(setq delete-old-versions -1)
(setq version-control t)
(setq vc-make-backup-files t)
(setq vc-follow-symlinks t)
(setq auto-save-file-name-transforms `((".*" ,(concat
                                               home-directory
                                               ".emacs.d/auto-save-list/"))))

(defun kill-current-buffer ()
  (interactive)
  (kill-buffer nil))

(let ((path (concat home-directory ".emacs.d/desktop/")))
  (unless (file-exists-p path)
    (make-directory path))
  (desktop-save-mode 1)
  (setq desktop-path
        `(,path "~"))
  (setq desktop-dirname path)
  (setq desktop-base-file-name "emacs-desktop"))

(defun shell-command-to-list (command)
  "Execute shell command COMMAND and return its output as a list of strings."
  (split-string (shell-command-to-string command)
                "\n" t "[ \f\t\n\r\v]+"))

(let ((emacs (shell-command-to-list "pgrep emacs"))
      (desktop-file ".emacs.d/desktop/.emacs.desktop.lock"))
  (when (and (eq (length emacs) 1)
             (file-exists-p (concat home-directory desktop-file)))
    (delete-file desktop-file)))

(use-package dired
  :ensure nil
  :defer 1
  :commands (dired dired-jump)
  :config
  (setq dired-dwim-target t
        dired-omit-failes "^\\.[^.].*"
        dired-listing-switches "-agho --group-directories-first"
        dired-omit-verbose nil
        dired-hide-details-hide-symlink-targets nil)
  (autoload 'dired-omit-mode "dired-x")
  (add-hook 'dired-load-hook
            (lambda ()
              (interactive)
              (dired-collapse)))
  (add-hook 'dired-mode-hook
            (lambda ()
              (interactive)
              (dired-omit-mode 1)
              (dired-hide-details-mode 1)
              (hl-line-mode 1))))

(use-package dired-rainbow
  :defer 2
  :config
  (dired-rainbow-define-chmod directory "#6cb2eb" "d.*")
  (dired-rainbow-define html "#eb5286" ("css" "less" "sass" "scss" "htm" "html" "jhtm" "mht" "eml" "mustache" "xhtml"))
  (dired-rainbow-define xml "#f2d024" ("xml" "xsd" "xsl" "xslt" "wsdl" "bib" "json" "msg" "pgn" "rss" "yaml" "yml" "rdata"))
  (dired-rainbow-define document "#9561e2" ("docm" "doc" "docx" "odb" "odt" "pdb" "pdf" "ps" "rtf" "djvu" "epub" "odp" "ppt" "pptx"))
  (dired-rainbow-define markdown "#ffed4a" ("org" "etx" "info" "markdown" "md" "mkd" "nfo" "pod" "rst" "tex" "textfile" "txt"))
  (dired-rainbow-define database "#6574cd" ("xlsx" "xls" "csv" "accdb" "db" "mdb" "sqlite" "nc"))
  (dired-rainbow-define media "#de751f" ("mp3" "mp4" "mkv" "MP3" "MP4" "avi" "mpeg" "mpg" "flv" "ogg" "mov" "mid" "midi" "wav" "aiff" "flac"))
  (dired-rainbow-define image "#f66d9b" ("tiff" "tif" "cdr" "gif" "ico" "jpeg" "jpg" "png" "psd" "eps" "svg"))
  (dired-rainbow-define log "#c17d11" ("log"))
  (dired-rainbow-define shell "#f6993f" ("awk" "bash" "bat" "sed" "sh" "zsh" "vim"))
  (dired-rainbow-define interpreted "#38c172" ("py" "ipynb" "rb" "pl" "t" "msql" "mysql" "pgsql" "sql" "r" "clj" "cljs" "scala" "js"))
  (dired-rainbow-define compiled "#4dc0b5" ("asm" "cl" "lisp" "el" "c" "h" "c++" "h++" "hpp" "hxx" "m" "cc" "cs" "cp" "cpp" "go" "f" "for" "ftn" "f90" "f95" "f03" "f08" "s" "rs" "hi" "hs" "pyc" ".java"))
  (dired-rainbow-define executable "#8cc4ff" ("exe" "msi"))
  (dired-rainbow-define compressed "#51d88a" ("7z" "zip" "bz2" "tgz" "txz" "gz" "xz" "z" "Z" "jar" "war" "ear" "rar" "sar" "xpi" "apk" "xz" "tar"))
  (dired-rainbow-define packaged "#faad63" ("deb" "rpm" "apk" "jad" "jar" "cab" "pak" "pk3" "vdf" "vpk" "bsp"))
  (dired-rainbow-define encrypted "#ffed4a" ("gpg" "pgp" "asc" "bfe" "enc" "signature" "sig" "p12" "pem"))
  (dired-rainbow-define fonts "#6cb2eb" ("afm" "fon" "fnt" "pfb" "pfm" "ttf" "otf"))
  (dired-rainbow-define partition "#e3342f" ("dmg" "iso" "bin" "nrg" "qcow" "toast" "vcd" "vmdk" "bak"))
  (dired-rainbow-define vc "#0074d9" ("git" "gitignore" "gitattributes" "gitmodules"))
  (dired-rainbow-define-chmod executable-unix "#38c172" "-.*x.*"))

(use-package dired-single
  :defer t)
(use-package dired-ranger
  :defer t)
(use-package dired-collapse
  :defer t)


;; Revert Dired and other buffers
(setq global-auto-revert-non-file-buffers t)

;; Revert buffers when the underlying file has changed
(global-auto-revert-mode 1)

(use-package openwith
  :config
  (setq openwith-associations
        (list
         (list (openwith-make-extension-regexp
                '("mpg" "mpeg" "avi" "wmv" "mov" "flv" "mkv" "mp4"))
               "vlc"
               '(file))
         (list (openwith-make-extension-regexp
                '("xbm" "pbm" "pgm" "ppm" "pnm" "png" "gif" "bmp" "tif" "jpeg" "jpg"))
               "feh"
               '(file))
         (list (openwith-make-extension-regexp
                '("xcf"))
               "gimp"
               '(file)))))

(use-package mu4e
  :ensure nil
  :config
  (setq
   mu4e-maildir "~/.local/var/spool/mail"
   mu4e-sent-folder "/Sent"
   mu4e-drafts-folder "/Drafts"
   mu4e-trash-folder "/Junk"
   mu4e-refile-folder "/Archive")
  (add-to-list 'mu4e-view-actions '("ViewInBrowser" . mu4e-action-view-in-browser) t)
  (setq mu4e-compose-signature (concat  "Kind regards,
  "
                                        user-full-name
                                        "
 PGP: 32D8 0C4C CA45 E66E 48D2  CAC0 C917 EE00 990B 6520"))
  (add-to-list 'mu4e-bookmarks
               '(
                 "flag:unread AND NOT flag:list"
                 "Non-mailing lists"
                 ?b))
  (add-to-list 'mu4e-bookmarks
               '(
                 "flag:unread AND list:debian-*"
                 "Debian mailing lists"
                 ?d))
  (add-to-list 'mu4e-bookmarks
               '(
                 "flag:unread AND list:linux-*"
                 "Linux mailing lists"
                 ?l))
  (add-to-list 'mu4e-bookmarks
               '(
                 "*Property* OR *property* OR *estate* OR *Estate*"
                 "Property"
                 ?l))
  (add-to-list 'mu4e-bookmarks
               ` (
                  ,(concat "from:"
                           user-full-name
                           " AND NOT from:"
                           university-email-address)
                  "My replies"
                  ?r))
  (add-to-list 'mu4e-bookmarks
               '(
                 "flag:flagged"
                 "Flagged"
                 ?f))
  (add-to-list 'mu4e-bookmarks
               `(
                 ,(concat "from:" university-email-address)
                 "University"
                 ?n))
  (add-to-list 'mu4e-bookmarks
               `(
                 ,(concat "NOT from:"
                          user-full-name
                          " AND NOT flag:replied AND NOT flag:list")
                 "Not yet replied"
                 ?s)))

(setq user-mail-address user-email-address)

(setq send-mail-function (quote mailclient-send-it))
(setq message-send-mail-function 'smtpmail-send-it
      smtpmail-stream-type 'starttls
      smtpmail-default-smtp-server "smtp-mail.outlook.com"
      smtpmail-smtp-server "smtp-mail.outlook.com"
      smtpmail-smtp-service 587
      smtpmail-starttls-credentials '(("smtp-mail.outlook.com"
                                       587 nil nil)))

(defun my/mu4e-open-pdf ()
  (interactive)
  (mu4e-open-attachment-emacs (current-buffer) 1))

(defun dw/evil-hook ()
  (dolist (mode '(custom-mode
                  eshell-mode
                  git-rebase-mode
                  erc-mode
                  circe-server-mode
                  circe-chat-mode
                  circe-query-mode
                  sauron-mode
                  term-mode
                  mingus-playlist
                  mingus-browse
                  disk-usage-mode))
    (add-to-list 'evil-emacs-state-modes mode)))

(use-package undo-tree
  :init
  (global-undo-tree-mode 1)
  (setq undo-tree-history-directory-alist `(("." . ,(concat home-directory ".emacs.d/undo-tree")))))

(use-package evil
  :init
  (setq evil-want-integration t)
  (setq evil-want-keybinding nil)
  (setq evil-want-C-u-scroll t)
  (setq evil-want-C-i-scroll nil)
  (setq evil-respect-visual-line-mode t)
  (setq evil-undo-system 'undo-tree)
  :config
  (add-hook 'evil-mode-hook 'dw/evil-hook)
  (evil-mode 1)
  (define-key evil-insert-state-map (kbd "C-g") 'evil-normal-state)
  (define-key evil-insert-state-map (kbd "C-h") 'evil-delete-backward-char-and-join)

  (evil-global-set-key 'motion "j" 'evil-next-visual-line)
  (evil-global-set-key 'motion "k" 'evil-previous-visual-line)

  (evil-set-initial-state 'messages-buffer-mode 'normal)
  (evil-set-initial-state 'dashboard-mode 'normal))

(use-package evil-collection
  :after evil
  :init
  (setq evil-collection-company-use-tng nil)
  :custom
  (evil-collection-outline-bind-tab-p nil)
  :config
  (setq evil-collection-mode-list
        (remove 'lispy evil-collection-mode-list))
  (evil-collection-init))


(use-package use-package-chords
  :disabled
  :config (key-chord-mode 1))

(require 'ansi-color)

(add-hook 'shell-mode-hook 'ansi-color-for-comint-mode-on)


(defadvice display-message-or-buffer (before ansi-color activate)
  "Process ANSI color codes in shell output."
  (let ((buf (ad-get-arg 0)))
    (and (bufferp buf)
         (string= (buffer-name buf) "*Shell Command Output*")
         (with-current-buffer buf
           (ansi-color-apply-on-region (point-min) (point-max))))))

(add-to-list 'auto-mode-alist '("\\.m$" . octave-mode))

(add-to-list 'auto-mode-alist '("\\.cob$" . cobol-mode))

(add-to-list 'auto-mode-alist '("^CMakeLists.txt$" . cmake-mode))

(setq-default tab-width 2)
(setq-default evil-shift-width tab-width)
(setq column-number-mode t)

(add-to-list 'default-frame-alist '(fullscreen . maximized))

(setq tls-checktrust t)
(setq tls-program
      (quote
       ("gnutls-cli --x509cafile %t -p %p %h"
        "gnutls-cli --x509cafile %t -p %p %h --protocols ssl3")))

(set-charset-priority 'unicode)
(setq locale-coding-system 'utf-8)
(set-terminal-coding-system 'utf-8)
(set-keyboard-coding-system 'utf-8)
(set-selection-coding-system 'utf-8)
(prefer-coding-system 'utf-8)
(setq default-process-coding-system '(utf-8-unix . utf-8-unix))

(set-frame-parameter (selected-frame) 'alpha '(90 . 90))
(add-to-list 'default-frame-alist '(alpha . (90 . 90)))
(set-frame-parameter (selected-frame) 'fullscreen 'maximized)
(add-to-list 'default-frame-alist '(fullscreen . maximized))

(fset 'yes-or-no-p 'y-or-n-p)

(setq initial-scratch-message
      (format ";; Scratch buffer started on %s\n\n"
              (current-time-string)))

(setq confirm-kill-emacs 'yes-or-no-p)

(add-hook 'before-save-hook 'delete-trailing-whitespace)

(tool-bar-mode -1)
(menu-bar-mode -1)
(scroll-bar-mode -1)

(setq epg-pinentry-mode 'loopback)
(use-package keychain-environment
  :init
  (keychain-refresh-environment))

(defun rotate-windows ()
  "Rotate your windows"
  (interactive)
  (cond ((not (> (count-windows)1))
         (message "You can't rotate a single window!"))
        (t
         (setq i 1)
         (setq numWindows (count-windows))
         (while  (< i numWindows)
           (let* (
                  (w1 (elt (window-list) i))
                  (w2 (elt (window-list) (+ (% i numWindows) 1)))

                  (b1 (window-buffer w1))
                  (b2 (window-buffer w2))

                  (s1 (window-start w1))
                  (s2 (window-start w2))
                  )
             (set-window-buffer w1  b2)
             (set-window-buffer w2 b1)
             (set-window-start w1 s2)
             (set-window-start w2 s1)
             (setq i (1+ i)))))))

(if (equal system-type 'windows-nt)
    (progn (setq explicit-shell-file-name
                 "C:/Program Files/Git/bin/sh.exe")
           (setq shell-file-name "bash")
           (setq explicit-sh.exe-args '("--login" "-i"))
           (setenv "SHELL" shell-file-name)
           (add-hook 'comint-output-filter-functions 'comint-strip-ctrl-m)))

(use-package haskell-mode
  :init
  (setq haskell-interactive-popup-errors nil))

(use-package hy-mode
  :init
  (setq hy-shell-interpreter
        (concat home-directory
                ".local/share/python/virtualenvs/hy/bin/hy"))
  (setq python-shell-virtualenv-root
        (concat home-directory
                ".local/share/python/virtualenvs/hy")))

(use-package virtualenvwrapper
  :after (python-mode hy-mode)
  :init
  (venv-initialize-interactive-shells)
  (venv-initialize-eshell)
  (setq venv-location (concat home-directory ".local/share/python/virtualenvs")))
(use-package pyvenv)
(add-to-list 'package-selected-packages 'pyvenv)

(setq ibuffer-show-empty-filter-groups nil)
(setq ibuffer-saved-filter-groups
      '(("default"
         ("version control" (or (mode . svn-status-mode)
                                (mode . svn-log-edit-mode)
                                (name . "^\\*svn-")
                                (name . "^\\*vc\\*$")
                                (name . "^\\*Annotate")
                                (name . "^\\*git-")
                                (name . "^\\*vc-")
                                (mode . magit-auto-revert-mode)))
         ("emacs" (or (name . "^\\*scratch\\*$")
                      (name . "^\\*Messages\\*$")
                      (name . "^TAGS\\(<[0-9]+>\\)?$")
                      (name . "^\\*Help\\*$")
                      (name . "^\\*info\\*$")
                      (name . "^\\*Occur\\*$")
                      (name . "^\\*grep\\*$")
                      (name . "^\\*Compile-Log\\*$")
                      (name . "^\\*Backtrace\\*$")
                      (name . "^\\*Process List\\*$")
                      (name . "^\\*gud\\*$")
                      (name . "^\\*Man")
                      (name . "^\\*WoMan")
                      (name . "^\\*Kill Ring\\*$")
                      (name . "^\\*Completions\\*$")
                      (name . "^\\*tramp")
                      (name . "^\\*shell\\*$")
                      (name . "^\\*compilation\\*$")))
         ("emacs source" (or (mode . emacs-lisp-mode)
                             (filename . "/bin/emacs")))
         ("agenda" (or (name . "^\\*Calendar\\*$")
                       (name . "^diary$")
                       (name . "^\\*Agenda")
                       (name . "^\\*org-")
                       (name . "^\\*Org")
                       (mode . org-mode)
                       (mode . muse-mode)))
         ("latex" (or (mode . latex-mode)
                      (mode . LaTeX-mode)
                      (mode . bibtex-mode)
                      (mode . reftex-mode)))
         ("dired" (or (mode . dired-mode)))
         ("helm" (or (name . "^\\*helm-mode-.*")))
         ("shell" (or (mode . eshell-mode)
                      (mode . shell-mode))))))
(add-hook 'ibuffer-mode-hook
          (lambda ()
            (ibuffer-switch-to-saved-filter-groups "default")))

(setq ispell-dictionary "british")

(use-package json-mode)
(use-package flycheck)
(defun my-parse-jslinter-warning (warning)
  (flycheck-error-new
   :line (1+ (cdr (assoc 'line warning)))
   :column (1+ (cdr (assoc 'column warning)))
   :message (cdr (assoc 'message warning))
   :level 'error
   :buffer (current-buffer)
   :checker 'javascript-jslinter))
(defun jslinter-error-parser (output checker buffer)
  (mapcar 'parse-jslinter-warning
          (cdr (assoc 'warnings (aref (json-read-from-string output) 0)))))
(flycheck-define-checker javascript-jslinter
  "A JavaScript syntax and style checker based on JSLinter.

See URL `https://github.com/tensor5/JSLinter'."
  :command ("c:/Users/Felix/AppData/Roaming/npm/jslint" "--raw" source)
  :error-parser jslinter-error-parser
  :modes (js-mode js2-mode js3-mode))

(define-key global-map (kbd "RET")		'reindent-then-newline-and-indent)
(define-key global-map (kbd "C-c <escape>")	'evil-force-normal-state)
(define-key global-map (kbd "ESC <escape>")	'evil-force-normal-state)
(define-key global-map (kbd "C-c a")		'org-agenda)
(define-key global-map (kbd "C-c b")		'org-switchb)
(define-key global-map (kbd "C-c c")		'org-capture)
(define-key global-map (kbd "C-c l")		'org-store-link)
(define-key global-map (kbd "C-c j")		'org-clock-goto)
(define-key global-map (kbd "C-c s")		'magit-status)
(define-key global-map (kbd "C-c C-l")		'load-file)
(define-key global-map (kbd "C-x x")		'switch-to-next-buffer)
(define-key global-map (kbd "C-x w")		'switch-to-prev-buffer)
(define-key global-map (kbd "C-x ,")		'kill-current-buffer)
(define-key global-map (kbd "C-c k")		'screenshot)
(define-key global-map (kbd "C-c C-r")		'align-regexp)
(define-key global-map (kbd "C-c C-b")		'text/indent-buffer)
(define-key global-map (kbd "C-c C-x +")	'text/increment-number-decimal)
(define-key global-map (kbd "C-c ;")		'xref-find-definitions)
(define-key global-map (kbd "C-x C-b")		'ibuffer)
(define-key global-map (kbd "M-;")		'comint-dynamic-complete-filename)

(use-package ledger-mode :ensure t)

(use-package slime
  :ensure t
  :init
  (setq inferior-lisp-program "/usr/bin/sbcl")
  :config
  (setq slime-contribs '(slime-fancy))
  (load (concat home-directory ".local/share/quicklisp/clhs-use-local.el") t))

(use-package magit :ensure t
  :config
  (setq magit-process-finish-apply-ansi-colors t))

(use-package gitlab
  :init
  (setq gitlab-host "https://gitlab.com"))

(use-package mingus
  :init
  (add-hook 'mingus-hook 'evil-emacs-state)
  (add-hook 'mingus-playlist-mode-hook 'evil-emacs-state)
  (add-hook 'mingus-playlist-hook 'evil-emacs-state)
  (add-hook 'mingus-browse-hook 'evil-emacs-state)
  (evil-set-initial-state 'mingus 'emacs)
  (evil-set-initial-state 'mingus-playlist-mode 'emacs)
  (evil-set-initial-state 'mingus-playlist 'emacs)
  (evil-set-initial-state 'mingus-browse 'emacs))



(use-package org-ref
  :ensure t
  :after (org)
  :init
  (setq org-ref-default-bibliography
        (concat home-directory "ln/uni-b-phd/tex/bib/bibliography.bib"))
  (setq org-ref-pdf-directory
        (concat home-directory "ln/uni-b-phd/tex/bib/pdf/"))
  (setq bibtex-completion-library-path
        (concat home-directory "ln/uni-b-phd/tex/bib/pdf/"))
  (setq org-ref-bibliography-notes
        (concat home-directory "ln/uni-b-phd/org/bibliography.org"))
  :config
  (define-key org-mode-map (kbd "C-c ]") 'org-ref-cite-insert-helm))
(setq
 bibtex-completion-bibliography
 (list
  (concat home-directory "ln/uni-b-phd/tex/bib/bibliography.bib"))
 bibtex-completion-library-path (concat home-directory "ln/uni-b-phd/tex/bib/pdf/")
 bibtex-completion-notes-path "ln/uni-b-phd/tex/bib/notes/"
 bibtex-completion-notes-template-multiple-files "* ${author-or-editor}, ${title}, ${journal}, (${year}) :${=type=}: \n\nSee [[cite:&${=key=}]]\n"

 bibtex-completion-additional-search-fields '(keywords)
 bibtex-completion-display-formats
 '((article       . "${=has-pdf=:1}${=has-note=:1} ${year:4} ${author:36} ${title:*} ${journal:40}")
   (inbook        . "${=has-pdf=:1}${=has-note=:1} ${year:4} ${author:36} ${title:*} Chapter ${chapter:32}")
   (incollection  . "${=has-pdf=:1}${=has-note=:1} ${year:4} ${author:36} ${title:*} ${booktitle:40}")
   (inproceedings . "${=has-pdf=:1}${=has-note=:1} ${year:4} ${author:36} ${title:*} ${booktitle:40}")
   (t             . "${=has-pdf=:1}${=has-note=:1} ${year:4} ${author:36} ${title:*}"))
 bibtex-completion-pdf-open-function
 (lambda (fpath)
   (call-process "evince" nil 0 nil fpath)))
(setq org-ref-insert-link-function 'org-ref-insert-link-hydra/body
      org-ref-insert-cite-function 'org-ref-cite-insert-helm
      org-ref-insert-label-function 'org-ref-insert-label-link
      org-ref-insert-ref-function 'org-ref-insert-ref-link
      org-ref-cite-onclick-function (lambda (_) (org-ref-citation-hydra/body)))

(use-package evil-org
  :after (org)
  :init (add-hook 'org-mode-hook 'evil-org-mode)
  :config
  (evil-org-set-key-theme '(navigation
                            insert textobjects
                            additional calendar)))
(use-package evil-org-agenda
  :ensure nil
  :after (evil-org)
  :config
  (evil-org-agenda-set-keys))

(use-package ox-latex
  :ensure nil
  :after (org org-ref)
  :init
  (setq org-latex-pdf-process
        '("latexmk -pdf -pdflatex=\"xelatex -shell-escape -interaction=nonstopmode -8bit\" -shell-escape -interaction=nonstopmode -f %f"))
  )

(defun bh/verify-refile-target ()
  "Exclude todo keywords with a DONE state from refile targets.
SOURCE: http://doc.norang.ca/org-mode.html"
  (not (member (nth 2 (org-heading-components)) org-done-keywords)))

(defun rgh/filter-timestamp (timestamp backend info)
  (cond
   ((org-export-derived-backend-p backend 'latex)
    (replace-regexp-in-string "[<>]\\|[][]" "" timestamp))
   ((org-export-derived-backend-p backend 'html)
    (replace-regexp-in-string "&[1g]t;\\|[][]" "" timestamp))))

(eval-after-load 'ox '(add-to-list
                       'org-export-filter-timestamp-functions
                       #'rgh/filter-timestamp))

(setq org-agenda-sorting-strategy
      '((agenda category-keep priority-down time-up)
        (todo priority-down category-keep)
        (tags priority-down category-keep)
        (search category-keep)))

(add-to-list 'org-latex-classes '("org-report"
                                  "\\documentclass[12pt,a4paper]{report}
[PACKAGES]
\\usepackage[backend=biber,style=numeric, sorting=none]{biblatex}
\\usepackage{epsfig}
\\usepackage{etoolbox}
\\usepackage[multidot]{grffile}
\\usepackage{helvet}
\\usepackage{pdfpages}
\\usepackage{siunitx}
\\usepackage{multicol}
\\usepackage{xcolor}
\\usepackage[explicit]{titlesec}
\\usepackage{hyperref}
\\usepackage[a4paper,bindingoffset=1cm,left=2cm,right=2cm,top=2cm,bottom=2cm,footskip=.5cm]{geometry}
\\AtBeginEnvironment{quote}{\\vspace\\vspace{-\\topsep}\\itshape}
\\AtEndEnvironment{quote}{\\vspace{-\\topsep}\\ignorespacesafterend}
\\renewcommand{\\familydefault}{\\sfdefault}
\\usepackage{tikz}
\\usepackage{tikzpagenodes}
\\usepackage{scrlayer-scrpage}
\\usepackage{lipsum}
[EXTRA]
\\AtBeginEnvironment{quote}{\\vspace\\vspace{-\\topsep}\\itshape}
\\AtEndEnvironment{quote}{\\vspace{-\\topsep}\\ignorespacesafterend}
\\renewcommand{\\familydefault}{\\sfdefault}

\\pagestyle{scrheadings}

\\definecolor{titleblue}{HTML}{290283}

\\newcommand{\\jamanta}{\\tikz[remember picture,overlay]
\\draw [titleblue,line width=2mm]
(current page.south west)
rectangle
(current page.north east)
;}

\\newcommand{\\greenborders}{\\tikz[remember picture,overlay] \\draw [titleblue]
(current page text area.south west)
rectangle
(current page text area.north east)
;}

\\chead[\\jamanta]{\\jamanta} % for page borders

%\\chead[\\greenborders]{\\greenborders} %for margin borders
\\newbox\\TitleUnderlineTestBox
\\newcommand*\\TitleUnderline[1]
{%
\\bgroup
\\setbox\\TitleUnderlineTestBox\\hbox{\\colorbox{titleblue}\\strut}%
\\setul{\\dimexpr\\dp\\TitleUnderlineTestBox-.3ex\\relax}{.3ex}%
\\ul{#1}%
\\egroup
}
\\newcommand*\\SectionNumberBox[1]
{%
\\colorbox{titleblue}
{%
\\makebox[2.5em][c]
{%
\\color{white}%
\\strut
\\csname the#1\\endcsname
}%
}%
\\TitleUnderline{\\ \\ \\ }%
}
\\titleformat{\\chapter}
{\\huge\\bfseries\\sffamily\\color{titleblue}}
{\\SectionNumberBox{chapter}}
{0pt}
{\\TitleUnderline{#1}}
\\titleformat{\\section}
{\\Large\\bfseries\\sffamily\\color{titleblue}}
{\\SectionNumberBox{section}}
{0pt}
{\\TitleUnderline{#1}}
\\titleformat{\\subsection}
{\\large\\bfseries\\sffamily\\color{titleblue}}
{\\SectionNumberBox{subsection}}
{0pt}
{\\TitleUnderline{#1}}
"
                                  ("\\chapter{%s}" . "\\chapter*{%s}")
                                  ("\\section{%s}" . "\\section*{%s}")
                                  ("\\subsection{%s}" . "\\subsection*{%s}")
                                  ("\\subsubsection{%s}" . "\\subsubsection*{%s}")
                                  ("\\paragraph{%s}" . "\\paragraph*{%s}")
                                  ("\\subparagraph{%s}" . "\\subparagraph*{%s}")))

(add-to-list 'org-latex-classes
             '("org-article"
               "\\documentclass[11pt,a4paper]{article}
[PACKAGES]
[EXTRA]"
               ("\\section{%s}" . "\\section*{%s}")
               ("\\subsection{%s}" . "\\subsection*{%s}")
               ("\\subsubsection{%s}" . "\\subsubsection*{%s}")
               ("\\paragraph{%s}" . "\\paragraph*{%s}")
               ("\\subparagraph{%s}" . "\\subparagraph*{%s}")))
(add-to-list 'org-latex-classes '("org-ieeetran"
                                  "\\documentclass[a4paper,journal,11pt]{IEEEtran}
[PACKAGES]
\\usepackage[backend=biber,style=ieee,sorting=none]{biblatex}
\\usepackage[lofdepth, lotdepth]{subfig}
\\usepackage{listings}
\\usepackage{mathtools}
\\usepackage{color, colortbl, xcolor}
\\usepackage{siunitx}
[EXTRA]"
                                  ("\\section{%s}" . "\\section*{%s}")
                                  ("\\subsection{%s}" . "\\subsection*{%s}")
                                  ("\\subsubsection{%s}" . "\\subsubsection*{%s}")
                                  ("\\paragraph{%s}" . "\\paragraph*{%s}")
                                  ("\\subparagraph{%s}" . "\\subparagraph*{%s}")))
(add-to-list 'org-latex-classes '("org-ieeetran-conference"
                                  "\\documentclass[a4paper,conference]{IEEEtran}
[PACKAGES]
[EXTRA]"
                                  ("\\section{%s}" . "\\section*{%s}")
                                  ("\\subsection{%s}" . "\\subsection*{%s}")
                                  ("\\subsubsection{%s}" . "\\subsubsection*{%s}")
                                  ("\\paragraph{%s}" . "\\paragraph*{%s}")
                                  ("\\subparagraph{%s}" . "\\subparagraph*{%s}")))

(add-to-list 'org-latex-classes '("org-tandf"
                                  "\\documentclass[]{interact}
[PACKAGES]
\\usepackage[natbibapa,nodoi,nourl]{apacite}  % Citation support using apacite.sty. Commands using natbib.sty MUST be deactivated first!
\\usepackage[lofdepth, lotdepth]{subfig}
\\usepackage{listings}
\\usepackage{mathtools}
\\usepackage{color, colortbl, xcolor}
\\usepackage{siunitx}
[EXTRA]
\\setlength\\bibhang{12pt}  % To set the indentation in the list of references using apacite.sty. Commands using natbib.sty MUST be deactivated first!
\\renewcommand\\bibliographytypesize{\\fontsize{10}{12}\\selectfont}  % To set the list of references in 10 point font using apacite.sty. Commands using natbib.sty MUST be deactivated first!

\\theoremstyle{plain}% Theorem-like structures provided by amsthm.sty
\\newtheorem{theorem}{Theorem}[section]
\\newtheorem{lemma}[theorem]{Lemma}
\\newtheorem{corollary}[theorem]{Corollary}
\\newtheorem{proposition}[theorem]{Proposition}

\\theoremstyle{definition}
\\newtheorem{definition}[theorem]{Definition}
\\newtheorem{example}[theorem]{Example}

\\theoremstyle{remark}
\\newtheorem{remark}{Remark}
\\newtheorem{notation}{Notation}

"
                                  ("\\section{%s}" . "\\section*{%s}")
                                  ("\\subsection{%s}" . "\\subsection*{%s}")
                                  ("\\subsubsection{%s}" . "\\subsubsection*{%s}")
                                  ("\\paragraph{%s}" . "\\paragraph*{%s}")
                                  ("\\subparagraph{%s}" . "\\subparagraph*{%s}")))
(add-to-list 'org-latex-classes
             '("org-beamer"
               "\\documentclass[presentation]{beamer}
[DEFAULT-PACKAGES]
[PACKAGES]
[EXTRA]
\\usetheme{Copenhagen}
\\setbeamerfont{caption}{size=\\scriptsize}
\\usepackage{subfig}
\\definecolor{darkbrown}{rgb}{0.38823529411764707, 0.3411764705882353, 0.2235294117647059}
\\definecolor{lightbrown}{rgb}{0.8784313725490196, 0.8392156862745098, 0.7647058823529411}
\\definecolor{green}{rgb}{0.5686274509803921, 0.8, 0.5686274509803921}
\\definecolor{red}{rgb}{1.0, 0.5843137254901961, 0.5843137254901961}
\\definecolor{white}{rgb}{1.0, 1.0, 1.0}
\\setbeamercolor{structure}{fg=darkbrown}
\\usepackage{color, colortbl, xcolor}"
               ("\\chapter{%s}" . "\\chapter*{%s}")
               ("\\section{%s}" . "\\section*{%s}")
               ("\\subsection{%s}" . "\\subsection*{%s}")
               ("\\subsubsection{%s}" . "\\subsubsection*{%s}")
               ("\\paragraph{%s}" . "\\paragraph*{%s}")
               ("\\subparagraph{%s}" . "\\subparagraph*{%s}")))

(add-to-list 'org-latex-classes
             '("org-memoir"
               "\\documentclass[11pt,a4paper,oneside]{memoir}
[PACKAGES]
[EXTRA]
\\hypersetup{pdfborder=0 0 0}"
               ("\\chapter{%s}" . "\\chapter*{%s}")
               ("\\section{%s}" . "\\section*{%s}")
               ("\\subsection{%s}" . "\\subsection*{%s}")
               ("\\subsubsection{%s}" . "\\subsubsection*{%s}")
               ("\\paragraph{%s}" . "\\paragraph*{%s}")
               ("\\subparagraph{%s}" . "\\subparagraph*{%s}")))

(add-to-list 'org-latex-classes
             '("org-beamer-ubmc"
               "\\documentclass[presentation]{beamer}
[DEFAULT-PACKAGES]
[PACKAGES]
[EXTRA]
\\usepackage[super]{natbib}
\\settheme{Madrid}
\\setbeamerfont{caption}{size=\\scriptsize}
\\usepackage{subfig}
\\definecolor{lightblue}{rgb}{0.6862745098039216, 0.8862745098039215, 1.0}
\\definecolor{white}{rgb}{1.0, 1.0, 1.0}
\\definecolor{darkbrown}{rgb}{0.38823529411764707, 0.3411764705882353, 0.2235294117647059}
\\definecolor{lightbrown}{rgb}{0.8784313725490196, 0.8392156862745098, 0.7647058823529411}
\\definecolor{green}{rgb}{0.5686274509803921, 0.8, 0.5686274509803921}
\\definecolor{red}{rgb}{1.0, 0.5843137254901961, 0.5843137254901961}
\\institute{University of Birmingham Mountaineering and Climbing Club}
\\definecolor{white}{rgb}{1.0, 1.0, 1.0}
\\setbeamercolor{structure}{fg=lightblue}
\\usepackage{color, colortbl, xcolor}
\\hypersetup{linkcolor=blue, pdfborder=0 0 0}"
               ("\\chapter{%s}" . "\\chapter*{%s}")
               ("\\section{%s}" . "\\section*{%s}")
               ("\\subsection{%s}" . "\\subsection*{%s}")
               ("\\subsubsection{%s}" . "\\subsubsection*{%s}")
               ("\\paragraph{%s}" . "\\paragraph*{%s}")
               ("\\subparagraph{%s}" . "\\subparagraph*{%s}")))
(add-to-list 'org-latex-classes
             '("org-beamer-balads"
               "\\documentclass[presentation]{beamer}
[DEFAULT-PACKAGES]
[PACKAGES]
[EXTRA]
\\usepackage[super]{natbib}
\\usetheme{Madrid}
\\setbeamerfont{caption}{size=\\scriptsize}
\\usepackage{subfig}
\\definecolor{darkpurple}{rgb}{0.2, 0.047, 0.3098}
\\definecolor{lightpurple}{rgb}{0.3765, 0.145, 0.4745}
\\definecolor{lightblue}{rgb}{0.6862745098039216, 0.8862745098039215, 1.0}
\\definecolor{white}{rgb}{1.0, 1.0, 1.0}
\\definecolor{darkbrown}{rgb}{0.38823529411764707, 0.3411764705882353, 0.2235294117647059}
\\definecolor{lightbrown}{rgb}{0.8784313725490196, 0.8392156862745098, 0.7647058823529411}
\\definecolor{green}{rgb}{0.5686274509803921, 0.8, 0.5686274509803921}
\\definecolor{red}{rgb}{1.0, 0.5843137254901961, 0.5843137254901961}
\\institute{University of Birmingham Ballroom and Latin American Dance Society}
\\definecolor{white}{rgb}{1.0, 1.0, 1.0}
\\setbeamercolor{structure}{fg=darkpurple}
\\setbeamercolor{structure}{bg=lightpurple}
\\usepackage{color, colortbl, xcolor}
\\hypersetup{linkcolor=blue, pdfborder=0 0 0}"
               ("\\chapter{%s}" . "\\chapter*{%s}")
               ("\\section{%s}" . "\\section*{%s}")
               ("\\subsection{%s}" . "\\subsection*{%s}")
               ("\\subsubsection{%s}" . "\\subsubsection*{%s}")
               ("\\paragraph{%s}" . "\\paragraph*{%s}")
               ("\\subparagraph{%s}" . "\\subparagraph*{%s}")))
(add-to-list 'org-latex-classes
             '("org-beamer-nr"
               "\\documentclass[presentation]{beamer}
[DEFAULT-PACKAGES]
[PACKAGES]
[EXTRA]
\\usetheme{Madrid}
\\setbeamerfont{caption}{size=\\scriptsize}
\\usepackage{subfig}
\\definecolor{orange}{rgb}{0.941, 0.494, 0.153}
\\definecolor{grey}{rgb}{0.722, 0.722, 0.722}
\\definecolor{lightblue}{rgb}{0.6862745098039216, 0.8862745098039215, 1.0}
\\definecolor{white}{rgb}{1.0, 1.0, 1.0}
\\definecolor{green}{rgb}{0.5686274509803921, 0.8, 0.5686274509803921}
\\definecolor{red}{rgb}{1.0, 0.5843137254901961, 0.5843137254901961}
\\definecolor{white}{rgb}{1.0, 1.0, 1.0}
\\setbeamercolor{structure}{fg=orange}
\\setbeamercolor{structure}{bg=grey}
\\usepackage{color, colortbl, xcolor}
\\hypersetup{linkcolor=blue, pdfborder=0 0 0}"
               ("\\chapter{%s}" . "\\chapter*{%s}")
               ("\\section{%s}" . "\\section*{%s}")
               ("\\subsection{%s}" . "\\subsection*{%s}")
               ("\\subsubsection{%s}" . "\\subsubsection*{%s}")
               ("\\paragraph{%s}" . "\\paragraph*{%s}")
               ("\\subparagraph{%s}" . "\\subparagraph*{%s}")))
(add-to-list 'org-latex-classes
             '("org-beamer-uob"
               "\\documentclass[presentation]{beamer}
[DEFAULT-PACKAGES]
[PACKAGES]
[EXTRA]
\\usetheme{uob}
\\usepackage{subfig}
\\usepackage{color, colortbl, xcolor}
\\hypersetup{linkcolor=blue, pdfborder=0 0 0}"
               ("\\chapter{%s}" . "\\chapter*{%s}")
               ("\\section{%s}" . "\\section*{%s}")
               ("\\subsection{%s}" . "\\subsection*{%s}")
               ("\\subsubsection{%s}" . "\\subsubsection*{%s}")
               ("\\paragraph{%s}" . "\\paragraph*{%s}")
               ("\\subparagraph{%s}" . "\\subparagraph*{%s}")))

(setq org-agenda-sorting-strategy
      '((agenda time-up priority-down todo-state-down alpha-up)
        (todo priority-down category-keep)
        (tags priority-down category-keep)
        (search category-keep)))

(use-package org-super-agenda :ensure t)
(setq org-super-agenda-groups
      '(;; Each group has an implicit boolean OR operator between its selectors.
        (:name "Deadlines"
               :deadline t)
        (:name "Finished"
               :todo ("DONE" "CANCELLED")
               :order 7)
        (:name "Important"
               :face 'org-mode-line-clock-overrun
               ;; Single arguments given alone
               :and (:priority "A"
                               :time-grid nil))
        (:name "Waiting" :todo "WAITING" :order 8)  ; Set order of this section
        ;; Set order of multiple groups at once
        ;; Groups supply their own section names when none are given
        (:name "Low Priority"
               :todo ("SOMEDAY" "HOLD")
               :priority ("C" "D" "E")
               ;; Show this group at the end of the agenda (since it has the
               ;; highest number). If you specified this group last, items
               ;; with these todo keywords that e.g. have priority A would be
               ;; displayed in that group instead, because items are grouped
               ;; out in the order the groups are listed.
               :order 9)
        (:name "Regular"
               :face 'org-archived
               :tag "REGULAR"
               :order 6)
        (:name "Today"  ; Optionally specify section name
               :time-grid t
               )  ; Items that appear on the time grid
        (:priority<= "B"
                     ;; Show this section after "Today" and "Important", because
                     ;; their order is unspecified, defaulting to 0. Sections
                     ;; are displayed lowest-number-first.
                     :order 1)
        ;; After the last group, the agenda will display items that didn't
        ;; match any of these groups, with the default order position of 99
        ))

(org-super-agenda-mode)

(setq org-publish-project-alist
      '(("org-blog"
         :base-directory "~/doc/projects/blog/org/"
         :base-extension "org"
         :publishing-directory "~/doc/projects/blog/jekyll/"
         :recursive t
         :exclude "blog.org"
         :publishing-function org-html-publish-to-html
         :headline-levels 4
         :html-extension "html"
         :body-only t)
        ("org-static-blog"
         :base-directory "~/doc/projects/blog/org/img/"
         :base-extension "jpg\\|png"
         :publishing-directory "~/doc/projects/blog/jekyll/assets/images/"
         :recursive t
         :publishing-function org-publish-attachment)
        ("blog" :components ("org-blog" "org-static-blog"))))

(defun org-list-tags (props)
  "Generate a list of tags from PROPS, the properties of an org entry."
  (let ((tags (cdr (assoc "TAGS" props))))
    (print tags)
    (if (null tags) nil
      (remove-if #'string-empty-p (split-string tags ":")))))

(defun string-delete-to-substring (string substring)
  "Delete all characters in STRING up to and including the first
  occurrence of SUBSTRING."
  (let ((index (search substring string)))
    (if (null index)
        string)
    (substring string index nil)))

(defun org-export-blog-entries ()
  "Export blog entries from file to separate files"
  (interactive)
  (save-excursion
    (let ((blog-file (expand-file-name
                      "blog.org"
                      (file-name-directory (buffer-file-name))))
          (posts-dir (expand-file-name
                      "_posts"
                      (file-name-directory (buffer-file-name))))
          (yaml-front-matter '(("layout" . "post") ("author" . user-full-name))))
      (find-file blog-file)
      (goto-char (point-min))
      (while (outline-next-heading)
        (print (point))
        (print (point-max))
        (org-map-tree
         (lambda ()
           (let* ((props (org-entry-properties))
                  (todo (cdr (assoc "TODO" props)))
                  (time (cdr (assoc "TIMESTAMP_IA" props)))
                  (tags (org-list-tags props)))
             (when (and todo time)
               (let* ((heading (org-get-heading t t t t))
                      (title (downcase (replace-regexp-in-string
                                        "[:=\(\)\?]" ""
                                        (replace-regexp-in-string
                                         "[ \t]" "-" heading))))
                      (str-time (and (string-match "\\([[:digit:]\-]+\\) " time)
                                     (match-string 1 time)))
                      (to-file (format "%s-%s.org" str-time title))
                      (org-buffer (current-buffer))
                      (yaml-front-matter (cons (cons "title" heading) yaml-front-matter))
                      html)
                 (org-narrow-to-subtree)
                 (setq html (buffer-string))
                 (set-buffer org-buffer) (widen)
                 (with-temp-file (expand-file-name to-file posts-dir)
                   (when yaml-front-matter
                     (insert "\#+OPTIONS: toc:nil\n")
                     (insert "\#+OPTIONS: num:nil\n")
                     (insert "\#+BEGIN_EXPORT html\n")
                     (insert "---\n")
                     (mapc (lambda (pair)
                             (insert (format "%s: %s\n" (car pair) (cdr pair))))
                           yaml-front-matter)
                     (unless (null tags)
                       (insert "categories:\n")
                       (mapc (lambda (tag)
                               (insert (format "    - %s\n" tag)))
                             tags))
                     (insert "---\n")
                     (insert "\#+END_EXPORT\n\n"))
                   (insert (string-delete-to-substring html "\n")))
                 (get-buffer org-buffer))))))
        ))))

(use-package helm-bibtex :ensure t)
(defun bibtex-insert-webpage-entry (url)
  (interactive "sURL? ")
  (insert (shell-command-to-string (concat "html_to_bibtex.py " url " html"))))


(defun org-ref-get-title ()
  "From a bibtex entry, return its title."
  (interactive)
  (save-excursion
    (bibtex-beginning-of-entry)
    (let* ((bibtex-expand-strings t)
           (entry (bibtex-parse-entry t)))
      (kill-new (reftex-get-bib-field "title" entry)))))

(defun org-ref-open-doi ()
  "From a bibtex entry, open the DOI webpage"
  (interactive)
  (save-excursion
    (bibtex-beginning-of-entry)
    (let ((doi (bibtex-autokey-get-field "doi")))
      (doi-utils-open doi))))

(defun org-ref-open-pdf ()
  "From a bibtex entry, open the PDF."
  (interactive)
  (save-excursion
    (bibtex-beginning-of-entry)
    (let ((key (bibtex-completion-get-key-bibtex)))
      (find-file (concat home-directory "ln/uni-b-pdf/" key ".pdf")))))

(defun org-ref-copy-downloaded-pdf ()
  "From a bibtex entry, move the downloaded pdf to the pdf/ folder."
  (interactive)
  (save-excursion
    (bibtex-beginning-of-entry)
    (let ((key (bibtex-completion-get-key-bibtex))
          (files (shell-command-to-list
                  (concat "find '" home-directory "down' -type f -name '*.pdf' | sort"))))
      (when files
        (shell-command (concat "mv '" (car files) "' " (concat home-directory "ln/uni-b-pdf/" key ".pdf")))
        (message "%s" (concat "Moved " key " to PDF directory"))
        ))))

(defun org-ref-doi-add-bibtex-entry (doi)
  (interactive "sDOI? ")
  (doi-utils-add-bibtex-entry-from-doi (string-trim doi) org-ref-default-bibliography))

(defun org-ref-crossref-add-bibtex-entry (query)
  (interactive "sQuery? ")
  (doi-utils-add-entry-from-crossref-query (string-trim query) org-ref-default-bibliography))


(define-key bibtex-mode-map (kbd "C-c o") 'org-ref-open-doi)
(define-key bibtex-mode-map (kbd "C-c m") 'org-ref-copy-downloaded-pdf)
(define-key bibtex-mode-map (kbd "C-c C-p") 'org-ref-open-pdf)


(defun rename-current-file (new-filename)
  "Rename current file to NEW-FILENAME within directory"
  (let* ((filename (buffer-file-name))
         (extension (file-name-extension filename)))
    (rename-file
     filename (concat (file-name-directory filename)
                      new-filename "." extension))))

(defvar org-ref-current-bibliography
  nil
  "Current active bibliography file, for use with `org-ref-rename-current-file'.")

(defun org-ref-set-current-bibliography ()
  "Set the current active bibliography file, for use with `helm-bibtex'.
  This should be called in an `org-mode' file with an active bibliography."
  (interactive)
  (setq org-ref-current-bibliography (file-truename (first (org-ref-find-bibliography)))))

(defun org-ref-rename-current-file ()
  "Rename current file to REF-NAME, provided interactively."
  (interactive)
  (let ((label (org-ref-label)))
    (unless (string-empty-p label)
      (rename-current-file label)
      (kill-current-buffer))))

(defun org-ref-label (&optional dummy)
  "Return the label of a bibtex entry."
  (interactive "P")
  (let* ((bibtex-completion-bibliography org-ref-current-bibliography)
         (label nil))
    (save-excursion
      (find-file "/tmp/org-ref.tmp")
      (helm-bibtex)
      (setq label (buffer-string))
      (set-buffer-modified-p nil)
      (kill-buffer (current-buffer)))
    label))


(setq *scopus-api-key* "3e39e36644788099efbce818cf77b06a")

(setq org-ref-bibtex-journal-abbreviations
      '(
        ("AEI" "Advanced Engineering Informatics" "Adv. Eng. Inf.")
        ("AJCE" "Australian Journal of Civil Engineering" "Aust. J. Civ. Eng.")
        ("CCP" "Civil-Comp Proceedings" "Civil-Comp Proc.")
        ("CS" "Computers and Structures" "Comput. Struct.")
        ("CEP" "Control Engineering Practice" "Control Eng. Pract.")
        ("CBM" "Construction and Building Materials" "Constr. Build. Mater.")
        ("EEJET" "Eastern-European Journal of Enterprise Technologies" "East.-Eur. J. Enterp. Technol.")
        ("ES" "Engineering Structures" "Eng. Struct.")
        ("ERR" "European Railway Review" "Eur. Railw. Rev.")
        ("ESA" "Expert Systems with Applications" "Expert Syst. Appl.")
        ("FBE" "Frontiers in Built Environment" "Front. Built Environ.")
        ("IA" "IEEE Access" "IEEE Access")
        ("IPC" "IEEE Pervasive Computing" "IEEE Pervas. Comput.")
        ("IPB" "IEE Proceedings B: Electric Power Applications" "IEE P. B: Electr. Power Appl.")
        ("IPCTA" "IEE Proceedings: Control Theory and Applications" "IEE P.: Control Theory Appl.")
        ("ITAE" "IEEE Transactions on Aerospace and Electronic Systems" "IEEE Trans. Aerosp. Electron. Syst.")
        ("ITPAMI" "IEEE Transactions on Pattern Analysis and Machine Intelligence" "IEEE Trans. Pattern Anal. Mach. Intell.")
        ("ITIP" "IEEE Transactions on Image Processing" "IEEE Trans. Image Process.")
        ("ITSP" "IEEE Transactions on Signal Processing" "IEEE Trans. Signal Process.")
        ("ITIE" "IEEE Transactions on Industrial Electronics" "IEEE Trans. Ind. Electron.")
        ("ITIM" "IEEE Transactions on Instrumentation and Measurement" "IEEE Trans. Instrum. Meas.")
        ("ITITS" "IEEE Transactions on Intelligent Transportation Systems" "IEEE Trans. Intell. Transp. Syst.")
        ("ITVT" "IEEE Transactions on Vehicular Technology" "IEEE Trans. Veh. Technol.")
        ("ITSMCC" "IEEE Transactions on Systems, Man and Cybernetics Part C: Applications and Reviews" "IEEE Trans. Syst., Man Cybern. Part C: Appl. Rev.")
        ("ISJ" "IEEE Sensors Journal" "IEEE Sens. J.")
        ("IOPCSMSE" "IOP Conference Series: Materials Science and Engineering" "IOP Conf. Ser.: Mater. Sci. Eng.")
        ("JAT" "Journal of Advanced Transportation" "J. Adv. Transp.")
        ("JGE" "Journal of Geotechnical Engineering" "J. Geotech. Eng.")
        ("JGGE" "Journal of Geotechnical and Geoenvironmental Engineering" "J. Geotech. Geoenviron. Eng.")
        ("JTE" "Journal of Transportation Engineering" "J. Transp. Eng.")
        ("JTG" "Journal of Transport Geography" "J. Transp. Geog.")
        ("JN" "Journal of Navigation" "J. Navigation")
        ("JS" "Journal of Sensors" "J. Sens.")
        ("JSV" "Journal of Sound and Vibration" "J. Sound Vib.")
        ("JTEA" "Journal of Transportation Engineering Part A: Systems" "J. Transp. Eng. Part A: Systems")
        ("JVE" "Journal of Vibroengineering" "J. Vibroeng.")
        ("JRPPWI" "Journal and Report of Proceedings of the Permanent Way Institution" "J. Rep. Proc.")
        ("JORS" "Journal of the Operational Research Society" "J. Oper. Res. Soc.")
        ("JMST" "Journal of Mechanical Science and Technology" "J. Mech. Sci. Technol.")
        ("JTTE" "Journal of Traffic and Transportation Engineering" "J. Traffic Transp. Eng.")
        ("IJC" "International Journal of Control" "Int. J. Control")
        ("IJCE" "International Journal of Civil Engineering" "Int. J. Civ. Eng.")
        ("IJRT" "International Journal of Rail Transportation" "Int. J. Rail Transp.")
        ("IJCTEE" "International Journal of Computer Technology and Electronics Engineering" "Int. J. Compu. Technol. Electron. Eng.")
        ("IJF" "International Journal of Fatigue" "Int. J. Fatigue")
        ("IJG" "International Journal of GEOMATE" "Int. J. GEOMATE")
        ("IJICIC" "International Journal of Innovative Computing, Information and Control" "Int. J. Innovative Comput. Inf. Control")
        ("IRJ" "International Railway Journal" "Int. Railw. J.")
        ("IJSS" "International Journal of Solids and Structures" "Int. J. Solids Struct.")
        ("IJSysS" "International Journal of Systems Science" "Int. J. Syst. Sci.")
        ("IJTDI" "International Journal of Transport Development and Integration" "Int. J. Transp. Dev. Integr.")
        ("IJDSN" "International Journal of Distributed Sensor Networks" "Int. J. Distrib. Sens. Networks")
        ("IJAME" "International Journal of Applied Mechanics and Engineering" "Int. J. Appl. Mech. Eng.")
        ("MBEC" "Medical & Biological Engineering & Computing" "Med. Biol. Eng. Comput.")
        ("TRPC" "Transportation Research Part C: Emerging Technologies" "Transp. Res. Part C: Emerg. Technol.")
        ("TRPD" "Transportation Research Part D: Transport and Environment" "Transp. Res. Part C: Transp. Environ.")
        ("TG" "Transportation Geotechnics" "Transp. Geotech.")
        ("TIG" "Transportation Infrastructure Geotechnology" "Transp. Infrastruct. Geotech.")
        ("TRP" "Transportation Research Procedia" "Transp. Res. Procedia")
        ("TRR" "Transportation Research Record: Journal of the Transportation Research Board" "Transp. Res. Rec.")
        ("TBA" "Transactions B: Applications" "Trans. B: Appl.")
        ("TAC" "Theoretical and Applied Climatology" "Theor. Appl. Climatol.")
        ("PSBS" "Procedia - Social and Behavioral Sciences" "Procedia Soc. Behav. Sci.")
        ("PE" "Procedia Engineering" "Procedia Eng.")
        ("PCS" "Procedia Computer Science" "Procedia Comput. Sci.")
        ("PIMEF" "Proceedings of the Institution of Mechanical Engineers Part F: Journal of Rail and Rapid Transit" "Proc. IMechE Part F: JRRT")
        ("PICE" "Proceedings of the Institution of Civil Engineers" "Proc. ICE")
        ("PICET" "Proceedings of the Institution of Civil Engineers - Transport" "Proc. ICE Transp.")
        ("PICEGE" "Proceedings of the Institution of Civil Engineers - Geotechnical Engineering" "Proc. ICE Geotech. Eng.")
        ("PIMEO" "Proceedings of the Institution of Mechanical Engineers Part O: Journal of Risk and Reliability" "Proc. IMechE Part O: JRR")
        ("PPTE" "Periodica Polytechnica, Transportation Engineering" "Period. Polytech., Transp. Eng.")

        ("MSSP" "Mechanical Systems and Signal Processing" "Mech. Syst. Signal Process.")
        ("MetApp" "Meteorological Applications" "Meteorol. Appl.")
        ("RTE" "Research in Transportation Economics" "R. Transp. Econ.")
        ("REIE" "Rail Engineering International Edition" "Rail Eng. Int. Ed.")
        ("RESS" "Reliability Engineering and System Safety" "Reliab. Eng. Syst. Saf.")
        ("SAAP" "Sensors and Actuators A: Physical" "Sens. Actuators, A")
        ("MPE" "Mathematical Problems in Engineering" "Math. Probl. Eng.")
        ("SIE" "Structure and Infrastructure Engineering" "Struct. Infrastruct. Eng.")
        ("VSD" "Vehicle System Dynamics" "Veh. Syst. Dyn.")
        ("WTBE" "WIT Transactions on the Built Environment" "WIT Trans. Built Environ.")
        ))


(defun org-ref-bibtex-generate-inline-shorttitles ()
  "Generate shorttitles.bib inline"
  (interactive)
  (append-to-file nil nil "shorttitles.bib")
  (dolist (row org-ref-bibtex-journal-abbreviations)
    (shell-command (concat
                    "sed -i 's/journal =.*" (nth 0 row)
                    ".*,/journal = {" (nth 2 row)
                    "},/' shorttitles.bib"))))

(setq org-agenda-files
      (append
       `(
         ,(concat uni-directory "academic/phd/org/phd.org")
         ,(concat home-directory "doc/projects/dance/org/dance.org")
         ,(concat home-directory "doc/personal/contact/contacts.org"))
       (directory-files-recursively (expand-file-name (concat home-directory "doc/personal/notes")) ".*.org$")))

(setq alert-default-style 'libnotify)

(use-package pcre2el)
(add-to-list 'package-selected-packages 'pcre2el)

(use-package lua-mode)

(use-package racket-mode)

(use-package paredit
  :ensure t
  :init
  (add-hook 'lisp-mode-hook 'paredit-mode)
  (add-hook 'emacs-lisp-mode-hook 'paredit-mode)
  (add-hook 'hy-mode-hook 'paredit-mode))

(use-package pdf-tools
  :ensure t
  :init
  (add-hook 'pdf-view-mode-hook (lambda () (pdf-view-midnight-minor-mode)))
  (setq pdf-view-midnight-colors '("#cccccc" . "#181a26"))
  (pdf-tools-install))

(load--function 'date/now)
(load--function 'lisp/save-last-kbd-macro)
(load--function 'help/display-random-mode-help)
(load--function 'img/screenshot)
(load--function 'mode/no-linum-mode)
(load--function 'text/indent-buffer)
(load--function 'text/increment-number)
(load--function 'org/contacts/get-email-address)
;(load--function 'org/contacts/org-contacts)
(load--function 'org/org-wiki-insert)

(load--function 'dfeich/org-clock-get-tr-for-ivl)
(load--function 'dfeich/org-slice-tr)
(load--function 'dfeich/org-clock-hourly-report)
(load--function 'dfeich/org-dblock-write:nagora-report)

(require 'sql)
(setq sql-database "gis")
(setq sql-server "localhost")
(setq sql-port 5432)
(setq sql-user "gisadmin")
(setq sql-postgres-options (quote ("-P" "pager=off")))
(defalias 'sql-get-login 'ignore)
(eval-after-load "sql"
  '(use-package sql-indent :ensure t))

(sql-set-product "postgres")

(require 'paren)
(show-paren-mode t)
(setq show-paren-delay 0)
(setq show-paren-style 'parenthesis)

(line-number-mode)
(column-number-mode)

(dolist (mode '(text-mode-hook
                prog-mode-hook
                conf-mode-hook))
  (add-hook mode (lambda () (display-line-numbers-mode 1))))

(dolist (mode '(org-mode-hook))
  (add-hook mode (lambda () (display-line-numbers-mode 0))))

(defvar linum-mode-inhibit-modes-list '(pdf-view-mode org-mode)
  "Modes to disable `linum-mode' in.")

(defadvice linum-on (around linum-on-inhibit-for-modes)
  "Stop loading `linum-mode' for some major modes."
  (unless (member major-mode linum-mode-inhibit-modes-list)
    ad-do-it))
(ad-activate 'linum-on)

(use-package ace-jump-mode
  :bind
  ("C-c SPC" . ace-jump-word-mode)
  ("C-c r" . ace-jump-char-mode)
  ("C-c i" . ace-jump-line-mode))

(require 'whitespace)
(setq whitespace-line-column 80)
(setq whitespace-style '(face lines-tail))
(whitespace-mode t)
(use-package highlight-indentation)

(use-package tramp
  :init
  (setq tramp-default-method "ssh"))

(use-package wttrin
  :ensure t
  :commands (wttrin)
  :init
  (setq wttrin-default-cities '("Birmingham"
                                "Aviemore"
                                "Llanberis"
                                "Coniston"
                                "Vancouver"
                                "Banff"))
  (setq wttrin-default-accept-language '("en-GB,en")))

(use-package yasnippet
  :ensure t
  :config
  (yas-global-mode t)
  (yas-reload-all)
  (add-hook 'prog-mode-hook 'yas-minor-mode)
  (add-hook 'text-mode-hook 'yas-minor-mode))

(use-package yasnippet-snippets
  :ensure t
  :after yasnippet)

(use-package csv-mode :ensure t)

(use-package json-mode :ensure t)

(use-package yaml-mode :ensure t)
(use-package origami
  :hook (yaml-mode . origami-mode))

(use-package hydra
  :ensure t
  :defer 1)

(defun kubectl-apply (file-name)
  (princ (shell-command-to-string (concat "kubectl apply -f " file-name))))

(defun kubectl-delete (file-name)
  (princ (shell-command-to-string (concat "kubectl delete -f " file-name))))

(defun kca-buffer () (interactive) (kubectl-apply (buffer-file-name)))
(defun kcd-buffer () (interactive) (kubectl-delete (buffer-file-name)))

(eval-after-load "yaml-mode"
  (lambda ()
    (define-key yaml-mode-map (kbd "C-c C-a") 'kca-buffer)
    (define-key yaml-mode-map (kbd "C-c C-d") 'kcd-buffer)))

(use-package posframe)
(use-package command-log-mode
  :after posframe)


(setq dw/command-window-frame nil)
(defun dw/toggle-command-window ()
  (interactive)
  (if dw/command-window-frame
      (progn
        (posframe-delete-frame clm/command-log-buffer)
        (setq dw/command-window-frame nil))
    (progn
      (global-command-log-mode t)
      (with-current-buffer
          (setq clm/command-log-buffer
                (get-buffer-create " *command-log*"))
        (text-scale-set -1))
      (setq dw/command-window-frame
            (posframe-show
             clm/command-log-buffer
             :position `(,(- (x-display-pixel-width) 650) . 50)
             :width 35
             :height 5
             :min-width 35
             :min-height 5
             :internal-border-width 2
             :internal-border-color "#c792ea"
             :override-parameters '((parent-frame . nil)))))))

(use-package keycast
  :config
  ;; This works with doom-modeline, inspired by this comment:
  ;; https://github.com/tarsius/keycast/issues/7#issuecomment-627604064
  (define-minor-mode keycast-mode
    "Show current command and its key binding in the mode line."
    :global t
    (if keycast-mode
        (add-hook 'pre-command-hook 'keycast-mode-line-update t)
      (remove-hook 'pre-command-hook 'keycast-mode-line-update)))
  (add-to-list 'global-mode-string '("" mode-line-keycast " ")))

(use-package calfw
  :commands cfw:open-org-calendar
  :config
  (setq cfw:fchar-junction ?╋
        cfw:fchar-vertical-line ?┃
        cfw:fchar-horizontal-line ?━
        cfw:fchar-left-junction ?┣
        cfw:fchar-right-junction ?┫
        cfw:fchar-top-junction ?┯
        cfw:fchar-top-left-corner ?┏
        cfw:fchar-top-right-corner ?┓)

  (use-package calfw-org
    :config
    (setq cfw:org-agenda-schedule-args '(:timestamp))))

(use-package rust-mode :ensure t)
(use-package rustic
  :ensure t
  :bind (:map rustic-mode-map
              ("M-j" . lsp-ui-imenu)
              ("M-?" . lsp-find-references)
              ("C-c C-c l" . flycheck-list-errors)
              ("C-c C-c a" . lsp-execute-code-action)
              ("C-c C-c r" . lsp-rename)
              ("C-c C-c q" . lsp-workspace-restart)
              ("C-c C-c Q" . lsp-workspace-shutdown)
              ("C-c C-c s" . lsp-rust-analyzer-status))
  :config
  (setq rustic-format-on-save t))

(use-package lsp-mode
  :commands lsp
  :custom
  (lsp-rust-analyzer-cargo-watch-command "clippy")
  (lsp-eldoc-render-all t)
  (lsp-idle-delay 0.6)
  (lsp-rust-analyzer-server-display-inlay-hints t)
  :config
  (add-hook 'lsp-mode-hook 'lsp-ui-mode))

(use-package lsp-ui
  :commands lsp-ui-mode
  :custom
  (lsp-ui-peek-always-show t)
  (lsp-ui-sideline-show-hover t)
  (lsp-ui-doc-enable nil))

(use-package company
  :custom
  (company-idle-delay 0.5)
  :init
  (global-company-mode)
  :config
  (setq company-backends
        '((company-files          ; files & directory
           company-keywords       ; keywords
           company-capf)  ; completion-at-point-functions
          (company-abbrev company-dabbrev)
          ))
  :bind
  (:map company-active-map
        ("C-n" . company-select-next)
        ("C-p" . company-select-previous)
        ("M-<" . company-select-first)
        ("M->" . company-select-last)))

(use-package projectile
  :config
  (projectile-mode +1)
  (define-key projectile-mode-map (kbd "C-c p j") 'projectile-command-map))

(use-package elpy
  :config
  (elpy-enable)
  (setq elpy-test-discover-runner-command '("python-shell-interpreter" "-m" "pytest"))
  (elpy-set-test-runner "py.test")
  (setq elpy-shell-echo-output nil)
  (setq elpy-rpc-timeout 2)
  (setq elpy-eldoc-show-current-function nil)
  (setq elpy-syntax-check-command "ruff")
  (setq elpy-formatter "ruff")
  (setq elpy-rpc-virtualenv-path 'current))

(use-package blacken
  :ensure t
  :hook (python-mode . blacken-mode)
  :config
  (setq blacken-line-length '88))

(use-package python-docstring
  :ensure t
  :hook (python-mode . python-docstring-mode)
  )

(setq-default python-indent-offset 4)

(use-package jupyter)
(use-package ein)

(use-package latex-math-preview)

(load--function 'mode/tdd)
(global-set-key (kbd "<f5>") 'recompile)



(use-package ob-plantuml
  :ensure nil
  :init
  (setq org-plantuml-jar-path (concat home-directory ".local/bin/plantuml.jar"))
  (setq org-plantuml-exec-mode "jar"))


(setq plantuml-default-exec-mode 'jar)
(setq plantuml-exec-mode 'jar)
(setq plantuml-jar-path "/usr/share/plantuml/plantuml.jar")
(setq org-plantuml-jar-path "/usr/share/plantuml/plantuml.jar")
(add-to-list 'auto-mode-alist '("\\.puml\\'" . plantuml-mode))
(use-package markdown-mode :ensure t)

(use-package terraform-mode)

(use-package forge :after magit)

(defun scroll-both-windows ()
  (interactive)
  "Scroll both open windows."
  (scroll-other-window 5)
  (scroll-up 5)
  )
(define-key global-map (kbd "C-x C-'")		'scroll-both-windows)
(use-package dockerfile-mode)
(use-package typescript-mode)
(use-package yasnippet-snippets :ensure t)
(use-package all-the-icons :ensure t)
(use-package all-the-icons-dired :ensure t)
(use-package ess :ensure t)
