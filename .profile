#!/usr/bin/env sh
# Copyright (C) 2019-2022 Richard Horridge <rhorridge@hotmail.co.uk>
#
# This file is part of `dotfiles' - Configuration files for various
# applications.

# ~/.profile: executed by the command interpreter for login shells.
# This file is not read by bash(1), if ~/.bash_profile or ~/.bash_login
# exists.
# see /usr/share/doc/bash/examples/startup-files for examples.
# the files are located in the bash-doc package.

# the default umask is set in /etc/profile; for setting the umask
# for ssh logins, install and configure the libpam-umask package.
#umask 022

# if running bash
if [ -n "$BASH_VERSION" ]; then
    # include .bashrc if it exists
    if [ -f "$HOME/.bashrc" ]; then
	      . "$HOME/.bashrc"
    fi
fi

# set PATH so it includes user's private bin if it exists
if [ -d "$HOME/bin" ] ; then
    PATH="$HOME/bin:$PATH"
fi

#  PATH setup
export GOPATH="$HOME/.local/share/go"
export PATH="$HOME/.poetry/bin:$HOME/.cabal/bin:$HOME/.local/bin:$GOPATH/bin:$PATH"
export LD_LIBRARY_PATH="$HOME/.local/lib:/usr/local/lib:/usr/lib:$LD_LIBRARY_PATH"
export EDITOR="vim"

# Shell Aliases
# Editing
alias v='vim'
alias em="emacsclient -c -n --alternate-editor=\"\""
alias g,s='git status'
alias g,c='git add -A && git commit -am'
alias g,l='git log --oneline'

# Moving / copying safely
alias mvy='mv -i'
alias cpy='cp -i'

# Black background when locking screen
alias i3l='i3lock -c 000000'

# Necessary navigation functions
alias shit='cd ../..'
alias shitshit='cd ../../..'
alias shitfuck='cd ../../../..'

# ls output
alias ls='ls --color=auto'
alias ll='ls -la'
alias l.='ls -d .*'

function docker_logs() {
    if [ -z "$2" ]; then
        docker logs -f "$(docker-compose ps -q "$1")"
    else
        docker --context="$2" logs -f "$(docker-compose --context="$2" ps -q "$1")"
    fi
}

function dev_docker_logs() {
    docker_logs "$1" acumen-local-dev 2>&1
}

alias docker-compose='docker compose'
alias dctx='docker-compose --context=acumen-local-dev'
alias dcb='docker-compose build'
alias dcu='docker-compose up'
alias dcl='docker-compose logs'
alias dclf='docker-compose logs --tail=5 -f'


alias kc='kubectl'
alias kcg='kubectl get'
alias kcd='kubectl describe'
alias kcl='kubectl logs'
alias kclf='kubectl logs --tail=5 -f'

alias ghr="gh run list"
alias ghprc="gh pr create -a '@me'"
alias ghprm="gh pr merge -s -d"
alias ghrv="gh run view --log-failed"

#  Pinentry
GPG_TTY=$(tty)
export GPG_TTY="${GPG_TTY}"

SSH_ENV="${HOME}/.ssh/environment"

start_agent () {
	  ssh-agent | sed 's/^echo/#echo/' > "${SSH_ENV}"
	  chmod 600 "${SSH_ENV}"
    # shellcheck source=.ssh/environment
	  . "${SSH_ENV}" > /dev/null
	  ssh-add
		ssh-add ~/.ssh/id_rsa_nr
}

if [ -f "${SSH_ENV}" ]; then
    # shellcheck source=.ssh/environment
	  . "${SSH_ENV}" > /dev/null
    # shellcheck disable=SC2009
	  ps -ef | grep ${SSH_AGENT_PID} | grep ssh-agent$ > /dev/null || {
		    start_agent;
	  }
else
	  start_agent;
fi

. "${HOME}/.secret"
. "$HOME/.cargo/env"

# Merge a pull request, squash automatically, checkout main branch
# and delete previous branch
function glabmrm () {
    set -u
    set -o pipefail

    BRANCH="$(git rev-parse --abbrev-ref HEAD)"
    ORIGIN_BRANCH="origin/${BRANCH}"
    MAIN_BRANCH="master"
    ORIGIN_MAIN_BRANCH="origin/${MAIN_BRANCH}"

    glab mr merge -s -d
    git checkout "${MAIN_BRANCH}"
    git pull
    git branch -D "${BRANCH}"
    git branch -D --remote "${ORIGIN_BRANCH}"
}

# Poetry / Python setup
export PYTHON_KEYRING_BACKEND=keyring.backends.null.Keyring

export GTK_THEME="Adwaita:dark"
