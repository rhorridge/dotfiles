""""  Copyright (C) 2019-21 Richard Horridge <rhorridge@hotmail.co.uk>
""""
""""  This file is part of `dotfiles' - Configuration files for various
""""  applications.

if &compatible
   set nocompatible
endif

set background=dark
set nowrap
set scrolloff=2
set number
set showmatch
set showmode
set showcmd
set ruler
set cursorline
set title
set wildmenu
set wildignore=*.o,*.obj,*.bak,*.exe,*.py[co],*.swp,*~,*.pyc,.svn,.git
set laststatus=2
set matchtime=2
set matchpairs+=<:>

set esckeys
set ignorecase
set smartcase
set smartindent
set smarttab
set magic
set bs=indent,eol,start


set tabstop=2
set shiftwidth=2

set lazyredraw
set confirm
set hidden
set history=50
set mouse=v

set t_Co=256
set incsearch
set hlsearch
syntax on
